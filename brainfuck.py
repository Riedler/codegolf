def s(p,i=0):
	r=[]
	while i<len(p):
		x=p[i]
		if x=='[':x,i=s(p,i+1)
		if x==']':return r,i
		r.append(x);i+=1
	return r
def g(a,p,t):
	for c in p:
		if type(c)==list:
			while a[t]:t=g(a,c,t)
			continue
		if c in '><':t+=1-2*(c=='<');a.setdefault(t,0)
		if c=='.':print(end=chr(a[t]))
		if c in '+-':a[t]+=1-2*(c=='-')
	return t
[g({0:0},s(p),0) for p in __import__("sys").argv[1:]]